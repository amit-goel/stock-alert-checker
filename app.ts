import * as express from 'express'
import * as request from "request";
import * as admin from 'firebase-admin';
import {StockInfo} from "./models/stockInfo";

var serviceAccount = require('./firebase/ddfdd-93b36-firebase-adminsdk-648m4-c65bf28557.json');
const {Observable, interval} = require('rxjs');
const {map, mergeMap, startWith} = require('rxjs/operators');
const defaultApp = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

let getMessage = (stock) => {
        return {
            notification: {
                title: '$' + stock.symbol,
                body: `${stock.symbol} is ${stock.greaterThan ? 'over' : 'under'} alert price(${stock.last_trade_price})`
            },
            topic: 'Alerts'
        };
    },
    sendMsg = stock => {
        admin.messaging().send(getMessage(stock))
            .then(response => console.log('Successfully sent message:', response))
            .catch(error => console.log('Error sending message:', error));
    },
    fetchContent = url => {
        return Observable.create(observer => {
            request.get(url, (error, response, body) => {
                error ? observer.error('yolo') : observer.next({response: response, body: body});
                observer.complete();
            });
        });
    };

let stockInfo: StockInfo[] = [
    /*{symbol: 'AMD', actionPrice: 16.25, greaterThan: false},
    {symbol: 'MU', actionPrice: 56.35, greaterThan: false},
    {symbol: 'IQ', actionPrice: 33.5, greaterThan: false},
    {symbol: 'ORCL', actionPrice: 48.6, greaterThan: true},*/
     {symbol: 'BAC', actionPrice: 30.12, greaterThan: false},
    //{symbol: 'CRON', actionPrice: 7.25, greaterThan: true}
];

let getAlerts = item => {
        item.alert = !item.greaterThan ? item.last_trade_price <= item.actionPrice : item.last_trade_price > item.actionPrice;
        return item;
    },
    mergeItems = result => {
        return {...stockInfo.find(i => i.symbol === result.symbol), ...result};
    },
    processStockItem = results =>
        results.map(result => mergeItems(result))
            .map(item => getAlerts(item))
            .filter(item => item.alert);

let getSymbols = () => stockInfo.map(info => info.symbol),
    getUrl = () => map(() => 'https://api.robinhood.com/quotes/?symbols=' + getSymbols()),
    checkPrice = (body: any) => processStockItem(body.results),
    processTriggeredStock = stock => {
        console.log(`${stock.symbol} triggered`);
        sendMsg(stock);
    };

interval(20000)
    .pipe(startWith(0), getUrl(), mergeMap(fetchContent), map(res => checkPrice(JSON.parse(res.body))))
    .subscribe(
        (stocks) => stocks.forEach(stock => processTriggeredStock(stock)),
        e => console.log(e)
    );
/*

var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var sassMiddleware = require('node-sass-middleware');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var app = express();
let request = require("request"),
    rx = require('rxjs');

let router = express.Router();
    options = {
    method: 'GET',
    url: 'https://blockchain.info/ticker',
};

request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.send(body);
});



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

/!*!// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});*!/

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
*/
