Demo Stock Market Application using NodeJS, TypeScript, and RX Observables.

Example app demonstrating REST API requests delivered asynchronously using RXJS extensions

App will determine whether an instrument is above or below a certain price, and will trigger a firebase message
if it meets the conditional trigger.

