export interface StockInfo {
    symbol: string;
    actionPrice: number;
    currentPrice?: number;
    greaterThan: boolean;
}